FROM php:5.4-apache
USER root
RUN docker-php-ext-install calendar
RUN apt-get update
RUN apt-get -y install locales locales-all
RUN sed -i '/pt_BR.UTF-8/s/^# //g' /etc/locale.gen
RUN sed -i '/pt_BR.UTF-8/s/^# //g' /etc/locale.gen
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen
RUN locale-gen
ENV LANG pt_BR.UTF-8
ENV LANGUAGE pt_BR:pt
ENV LC_ALL pt_BR.UTF-8
